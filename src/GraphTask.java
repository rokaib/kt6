import java.util.*;
import java.util.stream.Collectors;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
   /**
    * graphCache is used to store the matrix of the previously created graph
    */
   private int[][] graphCache;
   /**
    * vertCache is used to store the amount of vertices in the previously created graph
    */
   private int vertCache;

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      System.out.println("Graph example:");
      Graph originalGraph = new Graph("G");
      originalGraph.createRandomSimpleGraph(10, 9);
      System.out.println(originalGraph);


      long startTime = System.nanoTime();
      List<List<Arc>> allVariations = originalGraph.getAllDepthFirstSearchVariations();
      long endTime = System.nanoTime();

      System.out.println();
      System.out.println("Number of depth first search variations:");
      System.out.println("amount of variations: " + allVariations.size());
      System.out.println("time to find all in ms: " + ((endTime - startTime) / 1000000));
      System.out.println("");

      System.out.println("radius of this:");
      System.out.println(originalGraph.shortestPath());
      System.out.println("");

      System.out.println("Inverted:");
      Graph invertedG = new Graph("-G");
      invertedG.invertCachedGraph();
      System.out.println(invertedG);
   }

   /**
    * Represents a vertex in a graph
    */
   class Vertex {

      private String id;
      protected Vertex next;
      protected Arc first;
      private int info = 0;

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      /**
       * Finds Arc connecting this and other vertex
       * @param to target vertex
       * @return arc connecting this and to
       * @throws IllegalArgumentException when no arc is found.
       * */

      private Arc getConnectedArc(Vertex to) {
         for (Arc arc : first.getConnectedArcs()) {
            if (arc.target == to) {
               return arc;
            }
         }
         throw new IllegalArgumentException("Could not find arc connecting " + this + " and " + to);
      }

      /**
       * Finds all vertices, which are connected to this vertex
       * directly with arcs.
       * @return list of Vertices connected to this.
       * */

      private List<Vertex> getConnectedVertices() {
         if (first == null) return Collections.emptyList();
         return first.getConnectedArcs()
                 .stream()
                 .map(arc -> arc.target)
                 .collect(Collectors.toList());
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /**
    * Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      protected Vertex target;
      protected Arc next;
      private int info = 0;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      /**
       * @return list of arcs connected to this arc.
       * */

      private List<Arc> getConnectedArcs() {
         List<Arc> arcs = new ArrayList<>();

         Arc current = this;
         while (current != null) {
            arcs.add(current);
            current = current.next;
         }

         return arcs;
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /**
    * Represents a graph consisting of Vertex objects and Arc objects.
    */
   class Graph {

      private String id;
      protected Vertex first;
      private int info = 0;
      private final int INFINITYvar = Integer.MAX_VALUE / 4;

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      /**
       * Get all vertices used in this graph.
       * @return list of these vertices. */

      public List<Vertex> getAllVertices() {
         List<Vertex> res = new ArrayList<>();
         Vertex current = first;

         while (current != null) {
            res.add(current);
            current = current.next;
         }

         return res;
      }

      /**
       * Creates a Vertex for this graph.
       * @param vid vertex id
       * @return created vertex object
       * */

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      /**
       * Creates an Arc for this graph
       * @param aid Arc id
       * @param from starting vertex
       * @param to end vertex
       * @return created Arc
       * */

      public Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            }
         }
      }

      /**
       * create a connected undirected tree with n vertices.
       * @param n number of vertices added to this graph
       */
      public void createTree (int n){
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               String aid = "a" + varray[i].toString() + "_"
                       + varray[i].toString();
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with given amount of vertices and edges.
       *
       * @param vertices number of vertices
       * @param edges number of edges
       */
      public void createRandomSimpleGraph(int vertices, int edges) {
         vertCache = vertices;
         if (vertices <= 0)
            return;
         if (vertices > 2500)
            throw new IllegalArgumentException("Too many vertices: " + vertices);
         if (edges < vertices - 1 || edges > vertices * (vertices - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + edges);
         first = null;
         createRandomTree(vertices);       // vertices-1 edges created here
         Vertex[] vert = new Vertex[vertices];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = edges - vertices + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * vertices);  // random source
            int j = (int) (Math.random() * vertices);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         graphCache = connected;
      }

      /**
       * Inverts the values of the cached graphs matrix
       * and creates a new Graph with the given matrix values.
       */
      public void invertCachedGraph(){
         if (vertCache < 1){ throw new RuntimeException("Cached vertix has an illegal value! vertCache == "+vertCache);}
         if (graphCache == null){throw new RuntimeException("Unable to retrieve cached graph matrix! graphCache == "
                 + Arrays.deepToString(graphCache));}
         if (graphCache.length != vertCache) {
            throw new RuntimeException("Anomaly detected while trying to match the matrix and vertices! | Vertix: "
                    + vertCache + " | " + "Matrix length: "+ graphCache.length +
                    " | Matrix: " + Arrays.deepToString(graphCache) + " |");}
         int n = vertCache;
         int[][] connected = graphCache;
         for (int i = 0; i < connected.length; i++){
            for (int j = 0; j < connected.length; j++){
               if (connected[i][j] == 1 && i != j){
                  connected[i][j] = 0;
               }
               else if (connected[i][j] == 0 && i != j){
                  connected[i][j] = 1;
               }

            }
         }
         createTree(n);
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         for (int i = 0; i < connected.length; i++){
            for (int j = connected.length-1; j >= 0; j--){
               if(connected[i][j] == 1){
                  Vertex vi = vert [i];
                  Vertex vj = vert [j];
                  createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
               }

            }
         }
         graphCache = null;
         vertCache = 0;
      }

      /**
       * Converts adjacency matrix to the matrix of distances.
       *
       * @param input int[][] - adjacency matrix
       * @return Distance Matrix
       * source: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       */
      public int[][] distanceAdjMatrixConvert(int[][] input) {

         int[][] tempArray = new int [info][info];

         if (info < 1) {
            return tempArray;
         }
         for (int i=0; i<info; i++) {
            for (int j=0; j<info; j++) {
               if (input[i][j] == 0) {
                  tempArray[i][j] = INFINITYvar;
               } else {
                  tempArray[i][j] = 1;
               }
            }
         }
         for (int i=0; i<info; i++) {
            tempArray[i][i] = 0;
         }
         return tempArray;
      }

      /**
       * Uses the distanceAdjMatrixConvert method and calculates the shortest paths using the Floyd-Warshall algorithm.
       *
       * <p>
       * source: https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/
       * source: https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       */
      public int shortestPath() {
         int pointA;
         int pointB;
         int pointC;
         int radius = 0;
         int[][] temp = distanceAdjMatrixConvert(createAdjMatrix());

         for (pointB = 0; pointB < info; pointB++) {
            for (pointA = 0; pointA < info; pointA++) {
               for (pointC = 0; pointC < info; pointC++) {
                  if (temp[pointA][pointB] + temp[pointB][pointC] < temp[pointA][pointC]) {
                     temp[pointA][pointC] = temp[pointA][pointB] + temp[pointB][pointC];
                     radius = temp[pointA][pointC];
                  }
               }
            }
         }
         return radius;
      }

      /**
       * Uses brute force to count all different depth-first search variants.
       * @throws IllegalArgumentException when graph does not contain any vertices.
       * @return number of different variants
       * */
      public int getAmountOfDFSVariations() {
         return getAllDepthFirstSearchVariations().size();
      }

      /**
       * Uses brute force to find all possible depth-first search variants.
       * Uses up to 256 threads. 1 thread for every starting vertex.
       * @throws IllegalArgumentException when graph does not contain any vertices.
       * @return list of different variants
       * */
      public List<List<Arc>> getAllDepthFirstSearchVariations() {

         List<List<Arc>> res = new ArrayList<>();

         List<Vertex> all = getAllVertices();

         if (all.size() == 0) throw new IllegalArgumentException(
                 "Graph must contain at least one Vertex! None found!");

         List<Thread> threads = new ArrayList<>();
         for (Vertex vertex : all) {

            while (threads.size() > 256) {
               List<Thread> toRemove = new ArrayList<>();
               for (Thread thread : threads) {
                  if (thread.isAlive()) continue;
                  toRemove.add(thread);
               }
               threads.removeAll(toRemove);
            }

            Thread thread = new Thread(() -> res.addAll(findAllDFSWaysStartingFrom(vertex)));
            threads.add(thread);
            thread.start();
         }

         while (threads.size() != 0) {
            try {
               threads.get(0).join();
            } catch (InterruptedException ignored) {}

            List<Thread> toRemove = new ArrayList<>();
            for (Thread thread : threads) {
               if (thread.isAlive()) continue;
               toRemove.add(thread);
            }
            threads.removeAll(toRemove);
         }
         return res;
      }
   }

   /**
    * Finds all possible depth first search variants from a starting vertex.
    * @param first starting vertex
    * @return all possible variations
    * */

   private static List<List<Arc>> findAllDFSWaysStartingFrom(Vertex first) {
      List<List<Arc>> paths = new ArrayList<>();
      Stack<Map<Vertex, List<Vertex>>> lookedOptions = new Stack<>();


      do {
         List<Vertex> currentPathOfVertices = new ArrayList<>();

         // stores current variation of depth first search
         List<Arc> currentPathOfArcs = new ArrayList<>();

         currentPathOfVertices.add(first);

         // holds vertices, which potentially have other children,
         // which are not checked yet.
         Stack<Vertex> looking = new Stack<>();
         looking.push(first);

         // holds all possible choices, which can be made after the last vertex.
         Map<Vertex, List<Vertex>> choicesForVertices = new HashMap<>();

         while (looking.size() != 0) {

            //last vertex, which was added to the path
            Vertex pathLastVertex = getLastElement(currentPathOfVertices);
            List<Vertex> possible = minus(
                    looking.peek().getConnectedVertices(), currentPathOfVertices);

            if (possible.size() > 0
                    && !choicesForVertices.containsKey(pathLastVertex)) {
               choicesForVertices.put(pathLastVertex, possible);

               // if last vertex is last one in the filter,
               // remove all before tried choices from possible choices list.
               if (lookedOptions.size() > 0
                       && getParentVertex(lookedOptions.peek())
                       == pathLastVertex) {
                  possible = minus(
                          possible, lookedOptions.peek().get(pathLastVertex));

                  // if last vertex is in filter, but is not the last filter,
                  // limits choice to one, which was chosen last time.
               } else if (lookedOptions.size() > 0
                       && containsVertexAsKey(lookedOptions, pathLastVertex)) {
                  possible = new ArrayList<>();
                  possible.add(getLastElement(getByKey(lookedOptions, pathLastVertex)));
               }
            }

            if (possible.size() > 0) {

               // if there is a new branching, which has not been detected before
               if (possible.size() > 1
                       && !containsVertexAsKey(lookedOptions, pathLastVertex)) {
                  lookedOptions.push(getEmptyMap(pathLastVertex));
               }


               currentPathOfArcs.add(looking.peek().getConnectedArc(possible.get(0)));
               looking.push(possible.get(0));
               currentPathOfVertices.add(possible.get(0));

            } else {
               looking.pop();
            }
         }

         backTrack(currentPathOfVertices, lookedOptions, choicesForVertices);
         paths.add(currentPathOfArcs);

      } while (lookedOptions.size() != 0);

      return paths;
   }

   /**
    * Analyzes found depth first search variation.
    * Inserts chosen vertexes to a filter in form of a map.
    * @param currentPath found depth first search variant
    * @param lookedOptions filter containing branches
    *                      and choices made on previous iterations
    * @param choicesForVertices map containing all choices available
    *                          for given vertex
    * */

   private static void backTrack(List<Vertex> currentPath,
                                 Stack<Map<Vertex, List<Vertex>>> lookedOptions,
                                 Map<Vertex, List<Vertex>> choicesForVertices) {

      // if there is a Vertex, where branching was found,
      // chosen vertex must be added to filter.
      for (int i = currentPath.size() - 1; i >= 0 ; i--) {
         Vertex currentVertex = currentPath.get(i);
         int finalI = i;
         lookedOptions
                 .stream()
                 .filter(map -> getParentVertex(map) == currentVertex
                         && !map.get(currentVertex).contains(currentPath.get(finalI + 1)))
                 .forEach(map -> map.get(currentVertex).add(currentPath.get(finalI + 1)));
      }

      // last filter in stack contains all possible choices,
      // which have been tried, then it must be removed.
      while (lookedOptions.size() != 0
              && getChildVertices(lookedOptions.peek())
              .containsAll(choicesForVertices.get(getParentVertex(lookedOptions.peek())))) {
         lookedOptions.pop();
      }
   }

   /**
    * Searches whole stack for a map containing given key
    * @param lookedOptions stack containing maps
    * @param key searching key
    * @return if map with given key was found
    * */

   private static boolean containsVertexAsKey(
           Stack<Map<Vertex, List<Vertex>>> lookedOptions,
           Vertex key) {

      return lookedOptions
              .stream()
              .anyMatch(map -> getParentVertex(map) == key);
   }

   private static List<Vertex> getByKey(Stack<Map<Vertex, List<Vertex>>> stack, Vertex key) {
      Optional<Map<Vertex, List<Vertex>>> optional = stack
              .stream()
              .filter(map -> map.containsKey(key))
              .findFirst();

      return optional.map(GraphTask::getChildVertices).orElse(null);
   }

   private static <T> List<T> minus(List<T> first, List<T> second) {
      return first
              .stream()
              .filter(f -> !second.contains(f))
              .collect(Collectors.toList());
   }

   private static Map<Vertex, List<Vertex>> getEmptyMap(Vertex vertex) {
      Map<Vertex, List<Vertex>> res = new HashMap<>();
      res.put(vertex, new ArrayList<>());
      return res;
   }

   private static Vertex getParentVertex(Map<Vertex, List<Vertex>> map) {
      return map.keySet().stream().findFirst().orElse(null);
   }

   private static List<Vertex> getChildVertices(
           Map<Vertex, List<Vertex>> map) {
      return map.get(getParentVertex(map));
   }

   private static <T> T getLastElement(List<T> list) {
      if (!list.isEmpty()) return list.get(list.size() - 1);
      return null;
   }
}
